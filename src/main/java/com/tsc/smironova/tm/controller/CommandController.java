package com.tsc.smironova.tm.controller;

import com.tsc.smironova.tm.api.controller.ICommandController;
import com.tsc.smironova.tm.api.service.ICommandService;
import com.tsc.smironova.tm.exception.system.UnknownArgumentException;
import com.tsc.smironova.tm.exception.system.UnknownCommandException;
import com.tsc.smironova.tm.model.Command;
import com.tsc.smironova.tm.util.NumberUtil;
import com.tsc.smironova.tm.util.SystemOutUtil;
import com.tsc.smironova.tm.util.ValidationUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showIncorrectArgument(final String arg) {
        throw new UnknownArgumentException(arg);
    }

    @Override
    public void showIncorrectCommand(final String command) {
        throw new UnknownCommandException(command);
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Svetlana Mironova");
        System.out.println("E-MAIL: smironova@tsconsulting.com");
        SystemOutUtil.printLine();
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
        SystemOutUtil.printLine();
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands)
            System.out.println(command);
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands)
            System.out.println(command.getName());
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String arg = command.getArgument();
            if (!ValidationUtil.isEmpty(arg))
                System.out.println(arg);
        }
    }

    @Override
    public void showInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("[SYSTEM INFO]");
        System.out.println("Available processors: " + processors);
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void exit() {
        SystemOutUtil.printExitMessage();
        System.exit(0);
    }

}

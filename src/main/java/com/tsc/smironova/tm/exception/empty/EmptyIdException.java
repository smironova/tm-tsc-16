package com.tsc.smironova.tm.exception.empty;

import com.tsc.smironova.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }

}

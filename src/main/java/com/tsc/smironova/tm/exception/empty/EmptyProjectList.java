package com.tsc.smironova.tm.exception.empty;

import com.tsc.smironova.tm.exception.AbstractException;

public class EmptyProjectList extends AbstractException {

    public EmptyProjectList() {
        super("No projects! Add new project...");
    }

}

package com.tsc.smironova.tm.api.service;

import com.tsc.smironova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
